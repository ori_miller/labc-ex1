/**
* This program checks the correctness of enclosures structure in a file at given path.
*/

#include <stdio.h>



#define MAX_LINE_SIZE 1002
#define MAX_FILE_SIZE 5000

/**
* Returns true iff given char is an opening parenthesis.
*/
int isOpenPar(char par) 
{
	
	if ((par == '<') || (par == '{') || (par == '(') || (par == '[')) 
	{
		return 1;
	}
	
	return 0;
}

/**
* Returns true iff given char is a closing parenthesis.
*/
int isClosePar(char par) 
{
	
	if ((par == '>') || (par == '}') || (par == ')') || (par == ']')) 
	{
		return 1;
	}
	
	return 0;
}


/**
* Returns true iff given pair of chars are matching parentheses.
*/
int isMatchPar(char openPar, char closePar) 
{

	if ((openPar == '(') && (closePar == ')')) 
	{
		return 1;
	}

	if ((openPar == '<') && (closePar == '>')) 
	{
		return 1;
	}

	if ((openPar == '[') && (closePar == ']')) 
	{
		return 1;
	}

	if ((openPar == '{') && (closePar == '}')) 
	{
		return 1;
	}

	return 0;
} 

/**
* Operates a line from a file.
* Gets a text line from file, a stack array and stack index (in current time).
* Checks the parentheses structure of this line, and returns 0 if
* the structure becomes illegal, and 1 if it's still legal.
*/
int checkLine(const char* line, char* stack, int* ind) 
{
	int i = 0;

	while(line[i] != '\0') 
	{ 
		
		if (isOpenPar(line[i]))   /* found open paranthesis. */
		{
			stack[*ind] = line[i];
			(*ind)++;
		}
	
		if (isClosePar(line[i]))   /* found closing paranthesis. */
		{ 

			if (*ind == 0 )   /* too much closing parantheses. */
			{ 
				return 0;
			}
				
			if (isMatchPar(stack[(*ind) - 1], line[i]))   /* matching parentheses. */
			{ 
				(*ind)--;
			} 
			else 
			{
				return 0;		/* not-matching parentheses. */
			}		
		}
	
		i++;
	}

	return 1;
}

/** 
* Gets path to file, and checks if its parentheses structure is legal.
* Returns 1 if it's legal, 0 if it's illegal, and -1 if opening or readig from file failed.
*/
int checkStructure(const char* path) 
{
	char line[MAX_LINE_SIZE];
	char stack[MAX_FILE_SIZE];

	int ind = 0;
	int tmp;
	
	FILE* fp = fopen(path, "r"); 

	if (fp == NULL) 
	{   
		printf("Error: file could not be opened or the path/file doesn't exist: %s\n", path);
		return -1;
	}

	while(!feof(fp)) 
	{
		line[0] = '\0';  /* emptying the array. */

		fgets(line, MAX_LINE_SIZE, fp); 

		if (ferror(fp))
		{
			printf("Error: reading from file failed: %s\n", path);
			return -1;
		}

		tmp = checkLine(line, stack, &ind);  /* check curr. line. */

		if (tmp == 0) /* illegal structure. */
		{
			fclose(fp);
			return 0;
		}
	}

	
	if (ind != 0)   /* the stack isn't empty by the EOF - illegal structure. */
	{  
		fclose(fp);
		return 0;
	}

	fclose(fp);

	return 1;
}


/**
* The main.
* Gets the input from command line (filr path), 
* and invokes appropriate functions on it, printing the result.
*/
int main(int argc, char** argv)
{ 
	int res ; 

	res = checkStructure(argv[1]);

	if (res == 0) 
	{
		printf("Bad Structure\n");
	} 
	else 
	{
		if (res == 1) 
		{
			printf("OK\n");
		}
	}

	return 0;
}
