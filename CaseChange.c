/**
* This program gets an alphanumeric char from the user.
* In case it got a lower case letter, it will convert it and print its upper case version,
* and vice versa.
* In case it got a digit, it will print its square.
* Any other char is considered to be illegal.
*  
*/

#include <stdio.h>

/**
* Gets a char. 
* If it's not alphanumeric, prints 'Invalid Input'.
* On a letter char, converts and prints lower case letter to its upper case version, 
* and vice versa.
* On a digit char, print its square.
*/
void convertChar(char c)
{

	if ((c >= '0') && (c <= '9')) 
	{
		int digit = c - '0';	/* getting the numeric value of a digit char. */
		printf("%d\n", digit * digit); 	
		return;
	}

	printf("%c->", c);

	if ((c >= 'a') && (c <= 'z')) 
	{
		printf("%c\n", c - 32); /* ascii range between lower and upper case letters is 32. */
		return;
	}

	if ((c >= 'A') && (c <= 'Z')) 
	{
		printf("%c\n", c + 32); /* ascii range between lower and upper case letters is 32. */
		return;
	}

	printf("Invalid Input\n");
}

/**
* The main.
* Gets the input from the user (char), 
* and invokes appropriate functions on it.
*/
int main(void)
{
	char c;

	scanf("%c", &c);

	convertChar(c);

	return 0;
}
