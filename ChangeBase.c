/** 
* Converts a number, given in one base (original),
* to other, given base.
*/


#include <stdio.h>
#include <math.h>


/** computes and returns base^exp. */
double myPow(double base, int exp)
{
	double result = 1;

	while (exp >= 1)
	{
		result *= base;
		exp--;
	}

	return result;
}

/** 
* converts number from various bases to base 10. 
* returns -1 on invalid input.
*/
long long convertToBaseTen(long long num, int base)
{
	int pwr = 0;
	int temp;
	long long res = 0;
	
	while (num > 0) 
	{	
		temp = num % 10;  

		if (temp >= base)  /* digit >= base. */
		{
			printf("invalid\n");
			return -1;
		}

		res += temp * myPow(base , pwr);  /* digit * curr. power of 10 */
				
		num = num / 10;
		pwr++;
	}

	return res;
}


/** 
* converts number from base 10 to various bases. 
*/
long long convertFromBaseTen(long long num, int base)
{
	int pwr = 0;
	int temp;
	long long res = 0;
	
	while (num > 0) 
	{	
		temp = num % base;
		num = num / base;

		res += temp * myPow(10, pwr);
		pwr++;
	}

	return res; 	
}


/**
* The main.
* Gets the input from the user (number, org. base, other base), 
* and prints number in new base.
*/
int main(void)
{
	long long num, result;
	int originalBase, newBase;

	scanf("%lld#%d#%d", &num, &originalBase, &newBase);

	result = convertToBaseTen(num, originalBase);

	if (result != -1)
	{
		result = convertFromBaseTen(result, newBase); /* the number in new base. */
		printf("%lld", result);
	}

	return 0;
} 

