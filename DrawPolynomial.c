/** 
* Gets coefficients of a 3rd degree polynomial,
* and prints it.
*/

#include <stdio.h>
#include <math.h>



#define MIN_X_COOR -35
#define MAX_X_COOR 35
#define MIN_Y_COOR -10
#define MAX_Y_COOR 10


/** computes and returns base^exp. */
double myPow(double base, int exp)
{
	double result = 1;

	while (exp >= 1)
	{
		result *= base;
		exp--;
	}

	return result;
}


/** prints a point in a plane (which is not on the poly.), by given format. */
void printPoint(double x, double y)
{
	
	if ((x == 0) && (y == 0))  /* (0,0) */
	{ 
		printf("+");
	}

	if ((x == 0) && (y != 0))  /* Y axis */
	{ 
		printf("|");
	}

	if ((x != 0) && (y == 0))  /* X axis */
	{ 
		printf("-"); 
	}

	if ((x != 0) && (y != 0))  /* not on axes */
	{ 
		printf(" "); 
	}
}


/** 
* Draws the poly., whose coeff. are given.
*/
void drawPolynom(double a, double b, double c, double d) 
{
	double val;
	int x, y;

	for (y = MAX_Y_COOR; y >= MIN_Y_COOR; y--) 
	{
		for (x = MIN_X_COOR; x <= MAX_X_COOR; x++) 
		{		
			val = a + x * b + myPow(x, 2.0) * c + myPow(x, 3.0) * d;  /* the value of the poly. in x. */

			if (fabs(val - y) < 0.5) 	
			{
				printf("*");
			}
			else /* not on poly. */
			{
				printPoint(x, y);
			}

		}	/* end of "row" */
	
		printf("\n");
	}

}

/** Gets from the user the coeff. of the poly. */
void getPolyCoeff(double* a, double* b, double* c, double* d) 
{
	
	printf ("Select a:\n");
	scanf ("%lf", a);

	printf ("Select b:\n");
	scanf ("%lf", b);

	printf ("Select c:\n");
	scanf ("%lf", c);

	printf ("Select d:\n");
	scanf ("%lf", d);
}


/** 
* The main.
* Calls methods to read the poly. coeff. and to print it.
*/
int main(void)
{
	double a, b, c, d;

	printf ("y(x)=a+b*x+c*x^2+d*x^3\n");

	getPolyCoeff(&a, &b, &c, &d);

	printf("y(x)=(%.3f)+(%.3f)*x+(%.3f)*x^2+(%.3f)*x^3\n", a, b, c, d);

	drawPolynom(a, b, c, d);

	return 0;
}